class AddIndexToCarts < ActiveRecord::Migration
  def change
    add_index :carts, :account_id
  end
end
