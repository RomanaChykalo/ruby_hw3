class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.datetime :cart_date

      t.timestamps null: false
    end
  end
end
