class Product < ActiveRecord::Base
    validates :name, :presence => true
    # validates :price, :only_integer => true, :numericality => {:greater_than => 0}
    belongs_to :cart
end
