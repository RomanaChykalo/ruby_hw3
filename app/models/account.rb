class Account < ActiveRecord::Base
    validates :name, presence: true, length: { minimum: 8 }
    validates :age, presence: true, :numericality => {:greater_than_or_equal_to => 18, :less_than_or_equal_to => 100}
    has_one :cart
end
